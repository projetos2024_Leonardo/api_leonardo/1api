const router = require('express').Router()
const alunoController = require('../controller/alunoController')
const teacherController = require('../controller/teacherController')
const JSONPlaceholderController = require('../controller/JSONPlaceholderController')

router.get('/teacher/', teacherController.getTeacher)
router.post('/cadastroaluno/', alunoController.postAluno)
router.put('/updateUser/', alunoController.updateUser)
router.delete('/deleteuser/:id', alunoController.deleteUser)
router.get("/external/", JSONPlaceholderController.getUsers);
router.get("/external/io", JSONPlaceholderController.getUsersWebSiteIO);
router.get("/external/net", JSONPlaceholderController.getUsersWebSiteNET);
router.get("/external/com", JSONPlaceholderController.getUsersWebSiteCOM);
router.get("/external/filter",JSONPlaceholderController.getCountDomain); 

module.exports = router