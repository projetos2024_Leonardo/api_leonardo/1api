const express = require('express')
const app = express()

class AppController {
    constructor() {
      this.express = express();
      this.middlewares();
      this.routes();
    }

    middlewares() {
      this.express.use(express.json());
    }

    routes() {
      const apiRoutes= require('./routes/apiRoutes')
      this.express.use('/teste/api/', apiRoutes);
      this.express.get("/health/", (_, res) => {
        res.send({ status: "APLICAÇÃO NO AR" });
      });
    }
  }

  module.exports = new AppController().express;